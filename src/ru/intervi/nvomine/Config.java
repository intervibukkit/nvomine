package ru.intervi.nvomine;

import java.io.File;
import java.util.HashMap;

import ru.intervi.littleconfig.ConfigLoader;
import ru.intervi.littleconfig.utils.Utils;

public class Config {
	Config() {
		load();
	}
	
	public boolean daynight = false; //днем вещи не выпадают
		public String world = "NewV";
		public int dut = 23000; //утро
		public int dve = 13500; //вечер
		public int timint = 2000; //интервал таймера в тиках
	
	public boolean points = false;
		public int delay = 5;
		
	public boolean bookhopper = false;
		public String date = "YYYY-MM-dd/HH:mm:ss";
		public String[] blackList = null;
	
	public boolean vsit = false;
	public HashMap<String, String[]> worlds = new HashMap<String, String[]>();
	
	public void load() {
		String sep = File.separator;
		String path = Utils.getFolderPath(this.getClass()) + sep + "NVomine";
		File dir = new File(path); //проверка папки
		if (!dir.isDirectory()) dir.mkdirs();
		path += sep + "config.yml";
		File file = new File(path);
		if (!file.isFile()) Utils.saveFile(this.getClass().getResourceAsStream("/config.yml"), file);
		
		ConfigLoader config = null;
		try {
			config = new ConfigLoader(file, false);
		} catch(Exception e) {e.printStackTrace();}
		if (config == null) return;
		
		daynight = config.getBoolean("daynight");
		world = config.getString("world");
		dut = config.getInt("dut");
		dve = config.getInt("dve");
		timint = config.getInt("timint");
		points = config.getBoolean("points");
		delay = config.getInt("delay");
		bookhopper = config.getBoolean("bookhopper");
		date = config.getString("date");
		blackList = config.getStringArray("blacklist");
		vsit = config.getBoolean("vsit");
		
		for (String a : config.getSectionVars("worlds")) worlds.put(a, config.getStringArrayInSection("worlds", a));
	}
}
