package ru.intervi.nvomine;

import java.io.File;

import org.bukkit.ChatColor;

import ru.intervi.littleconfig.ConfigLoader;
import ru.intervi.littleconfig.utils.Utils;

public class Mess {
	Mess() {
		load();
	}
	
	public String son = "&fсохранение инвентаря включено до ночи";
	public String soff = "&fсохранение инвентаря выключено до утра";
	public String point = "&cКоманда: &e%com%";
	public String help[] = {
			"---------<NVomine>---------",
			"/vom points on|off - включить режим обнаружения точек",
			"/vom points add command - добавить точку с командой",
			"- %name% - заменится на ник вошедшего игрока",
			"- %world% - мир (из локации того, кто ставит точку)",
			"- %x% - x координата, %y% - y, %z% - z",
			"/vom points remove - удалить точку",
			"/vom boom world x y z power setfire breakBlocks - создать взрыв",
			"- (/vom boom World 0 0 0 5 false false)",
			"/vom boom ник power setFire breakBlocks - взорвать игрока",
			"/vom hadd - добавить воронку принятия отзывов",
			"/vom hrem - удалить воронку"
	};
	public String noperm = "нет прав";
	public String reload = "конфиг перезагружен";
	public String seeon = "режим поиска точек включен";
	public String seeoff = "режим поиска точек выключен";
	public String ingame = "эту команду можно выполнять только из игры";
	public String padd = "точка добавлена";
	public String pthere = "тут уже есть точка";
	public String prem = "точка удалена";
	public String pnone = "тут нет точки";
	public String novar = "неверные значения";
	public String noworld = "нет такого мира";
	public String pexpl = "%name% взорван";
	public String expl = "взрыв создан";
	public String noexpl = "взрыв не удался";
	public String modoff = "модуль выключен";
	public String bhopper = "книга сохранена в файл";
	public String nohopper = "это не воронка";
	public String nohdel = "тут нет принимающей воронки";
	public String hdel = "воронка удалена";
	public String hadd = "воронка добавлена";
	
	private String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	public void load() {
		String sep = File.separator;
		String path = Utils.getFolderPath(this.getClass()) + sep + "NVomine";
		File dir = new File(path); //проверка папки
		if (!dir.isDirectory()) dir.mkdirs();
		path += sep + "mess.yml";
		File file = new File(path);
		if (!file.isFile()) Utils.saveFile(this.getClass().getResourceAsStream("/mess.yml"), file);
		
		ConfigLoader config = null;
		try {
			config = new ConfigLoader(file, false);
		} catch(Exception e) {e.printStackTrace();}
		if (config == null) return;
		
		son = color(config.getString("son"));
		soff = color(config.getString("soff"));
		point = color(config.getString("point"));
		help = config.getStringArray("help");
		for (int i = 0; i < help.length; i++) help[i] = color(help[i]);
		noperm = color(config.getString("noperm"));
		reload = color(config.getString("reload"));
		seeon = color(config.getString("seeon"));
		seeoff = color(config.getString("seeoff"));
		ingame = color(config.getString("ingame"));
		padd = color(config.getString("padd"));
		pthere = color(config.getString("pthere"));
		prem = color(config.getString("prem"));
		pnone = color(config.getString("pnone"));
		novar = color(config.getString("novar"));
		noworld = color(config.getString("noworld"));
		pexpl = color(config.getString("pexpl"));
		expl = color(config.getString("expl"));
		noexpl = color(config.getString("noexpl"));
		modoff = color(config.getString("modoff"));
		bhopper = color(config.getString("bhopper"));
		nohopper = color(config.getString("nohopper"));
		nohdel = color(config.getString("nohdel"));
		hdel = color(config.getString("hdel"));
		hadd = color(config.getString("hadd"));
	}
}
