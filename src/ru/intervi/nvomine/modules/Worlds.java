package ru.intervi.nvomine.modules;


import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import ru.intervi.nvomine.Main;

public class Worlds {
	public Worlds(Main m) {
		main = m;
	}
	
	private Main main;
	
	public void onChangeWorld(PlayerChangedWorldEvent event) {
		String from = event.getFrom().getName();
		String to = event.getPlayer().getLocation().getWorld().getName();
		if (!from.equals(to) && main.conf.worlds.containsKey(to)) {
			for (String cmd : main.conf.worlds.get(to)) {
				cmd = cmd.replaceAll("%name%", event.getPlayer().getName());
				cmd = cmd.replaceAll("%from%", from);
				cmd = cmd.replaceAll("%to%", to);
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
			}
		}
	}
}
