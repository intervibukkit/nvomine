package ru.intervi.nvomine.modules;

import ru.intervi.nvomine.Main;

import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.Material;
import org.bukkit.Location;

/**
 * сажание жителей в транспорт
 */
public class VSit {
	public VSit(Main m) {
		main = m;
	}
	
	private Main main;
	
	public void onInteractE(PlayerInteractEntityEvent event) {
		if (!main.conf.vsit) return;
		Entity entity = event.getRightClicked();
		if (!entity.getType().equals(EntityType.VILLAGER)) return;
		PlayerInventory inv = event.getPlayer().getInventory();
		EquipmentSlot slot = event.getHand();
		ItemStack item = null;
		if (slot.equals(EquipmentSlot.HAND)) item = inv.getItemInMainHand();
		else if (slot.equals(EquipmentSlot.OFF_HAND)) item = inv.getItemInMainHand();
		if (item == null) return;
		Location loc = entity.getLocation();
		if (item.getType().equals(Material.MINECART))
			loc.getWorld().spawnEntity(loc, EntityType.MINECART).setPassenger(entity);
		else if (item.getType().equals(Material.BOAT))
			loc.getWorld().spawnEntity(loc, EntityType.BOAT).setPassenger(entity);
		else return;
		if (slot.equals(EquipmentSlot.HAND)) inv.setItemInMainHand(new ItemStack(Material.AIR));
		else inv.setItemInOffHand(new ItemStack(Material.AIR));
		event.setCancelled(true);
	}
}
