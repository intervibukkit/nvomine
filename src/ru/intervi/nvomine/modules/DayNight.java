package ru.intervi.nvomine.modules;

import org.bukkit.World;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.entity.Player;

import ru.intervi.nvomine.Main;

/**
 * смена gamerule keepInventory в зависимости от времени
 */
public class DayNight {
	public DayNight(Main m) {
		main = m;
	}
	
	private Main main;
	private BukkitTask task = null;
	
	public void startTimer() { //старт таймера
		task = new Tim().runTaskTimer(
				Bukkit.getPluginManager().getPlugin("NVomine"), main.conf.timint, main.conf.timint);
	}
	
	public void stopTimer() { //остановка
		if (task != null) {
			task.cancel();
			task = null;
		}
	}
	
	private class Tim extends BukkitRunnable {
		int ch = 0;
		
		public void run() {
			World w = Bukkit.getWorld(main.conf.world);
			if (w == null) return;
			//23000 - утро
			//13500 - вечер
			long t = w.getTime();
			//включение сохранения инвентаря утром
			if ((t >= main.conf.dut || t < main.conf.dve) && ((ch == 2 || ch == 0) && main.conf.daynight)) {
				if (!w.setGameRuleValue("keepInventory", "true")) main.info("не удалось изменить game rule");
				for (Player p : w.getPlayers()) p.sendMessage(main.mess.son);
				//main.info("сохранение инвентаря включено до ночи");
				ch = 1;
			}
			//выключение вечером
			if ((t >= main.conf.dve && t < main.conf.dut) && ((ch == 1 || ch == 0) && main.conf.daynight)) {
				if (!w.setGameRuleValue("keepInventory", "false")) main.info("не удалось изменить game rule");
				for (Player p : w.getPlayers()) p.sendMessage(main.mess.soff);
				//main.info("сохранение инвентаря выключено до утра");
				ch = 2;
			}
		}
	}
}
