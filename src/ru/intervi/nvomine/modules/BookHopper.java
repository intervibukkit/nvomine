package ru.intervi.nvomine.modules;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.io.FileWriter;
import java.io.BufferedWriter;

import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.block.Block;
import org.bukkit.Location;
import org.bukkit.inventory.Inventory;
import org.bukkit.event.block.BlockBreakEvent;

import ru.intervi.littleconfig.utils.Utils;
import ru.intervi.littleconfig.FileStringList;
import ru.intervi.nvomine.Main;

/**
 * сохранение книг с отзывами в файл
 */
public class BookHopper {
	public BookHopper(Main m) {
		main = m;
		PATH = Utils.getFolderPath(this.getClass()) + File.separator + "NVomine" + File.separator + "hopper.txt";
		d = new SimpleDateFormat(main.conf.date);
		load();
	}
	
	private Main main;
	private final String PATH; //файл с локациями воронок
	private final String PATH_BOOKS = Utils.getFolderPath(this.getClass()) + File.separator + "NVomine" + File.separator +
			"books.txt"; //файл с книгами
	private SimpleDateFormat d = null; //формат даты
	private ArrayList<String> list = new ArrayList<String>(); //локации воронок
	
	/**
	 * загрузить список воронок
	 */
	public void load() {
		if (!(new File(PATH).isFile())) return;
		try {
			FileStringList fsl = new FileStringList(PATH);
			list = fsl.list;
		} catch(IOException e) {e.printStackTrace();}
	}
	
	private void save() { //сохранить список воронок
		if (list.isEmpty()) {
			new File(PATH).delete();
			return;
		}
		try {
			FileStringList fsl = new FileStringList();
			fsl.list = list;
			fsl.write(PATH);
		} catch(IOException e) {e.printStackTrace();}
	}
	
	private void saveBook(List<String> book) { //запись книги в файл
		if (book.isEmpty()) return;
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(PATH_BOOKS, true));
			for (String str : book) {
				writer.write(str);
				writer.newLine();
			}
		} catch(IOException e) {e.printStackTrace();}
		finally {
			try {if (writer != null) writer.close();}
			catch(IOException e) {e.printStackTrace();}
		}
	}
	
	public void clear() {
		list.clear();
	}
	
	/**
	 * добавить воронку в список принимающих
	 * @param loc локация игрока, стоящего на воронке
	 * @return true если добавлена; false если нет
	 */
	public boolean add(Location loc) {
		Block block = loc.getBlock().getRelative(0, -1, 0);
		if (!block.getType().equals(Material.HOPPER)) return false;
		String tloc = main.getLoc(block.getLocation());
		if (list.contains(tloc)) return false;
		list.add(tloc);
		save();
		return true;
	}
	
	/**
	 * удалить принимающую воронку
	 * @param loc локация игрока, стоящего на воронке
	 * @return true если удалена; false если нет
	 */
	public boolean remove(Location loc) {
		String tloc = main.getLoc(loc.getBlock().getRelative(0, -1, 0).getLocation());
		if (!list.contains(tloc)) return false;
		list.remove(tloc);
		save();
		return true;
	}
	
	public void invPickup(InventoryPickupItemEvent event) { //обработка принимающих воронок
		if (!main.conf.bookhopper) return;
		Inventory inv = event.getInventory();
		if (!inv.getType().equals(InventoryType.HOPPER)) return;
		if (!list.contains(main.getLoc(inv.getLocation()))) return;
		event.setCancelled(true); //чтобы не засоряли хламом
		Item item = event.getItem();
		ItemStack stack = event.getItem().getItemStack();
		Material type = stack.getType();
		if (!type.equals(Material.WRITTEN_BOOK) && !type.equals(Material.BOOK_AND_QUILL)) return;
		BookMeta meta = ((BookMeta) stack.getItemMeta());
		String author = null, title = null, page = null, display = null;
		List<String> pages = meta.getPages(), lore = meta.getLore();
		if ((pages != null && !pages.isEmpty()) || (lore != null && !lore.isEmpty())) {
			if (meta.hasAuthor()) author = meta.getAuthor();
			if (meta.hasTitle()) title = meta.getTitle();
			if (meta.hasPages()) page = String.valueOf(meta.getPageCount());
			if (meta.hasDisplayName()) display = meta.getDisplayName();
		} else if (author == null && title == null && display == null) return; //отсев пустых книг
		if (title != null) {
			for (String str : main.conf.blackList) {
				if (title.equals(str)) return;
			}
		}
		ArrayList<String> result = new ArrayList<String>();
		result.add("------------------< " + d.format(new Date()) + " >------------------");
		if (author != null) result.add("Автор: " + author);
		if (title != null) result.add("Название: " + title);
		if (page != null) result.add("Страниц: " + page);
		if (display != null) result.add("Имя предмета: " + display);
		if (lore != null && !lore.isEmpty()) {
			result.add("---< Lore >---");
			result.addAll(lore);
			result.add("---< /Lore >---");
		}
		if (pages != null && !pages.isEmpty()) {
			result.add("---< Текст книги >---");
			result.addAll(pages);
			result.add("---< /Текст книги >---");
		}
		saveBook(result);
		item.remove();
		main.info(main.mess.bhopper);
	}
	
	public void onBreak(BlockBreakEvent event) { //удаление локакий разрушенных воронок
		if (!main.conf.bookhopper) return;
		Block block = event.getBlock();
		if (!block.getType().equals(Material.HOPPER)) return;
		String tloc = main.getLoc(block.getLocation());
		if (!list.contains(tloc)) return;
		list.remove(tloc);
		save();
		event.getPlayer().sendMessage(main.mess.hdel);
	}
}
