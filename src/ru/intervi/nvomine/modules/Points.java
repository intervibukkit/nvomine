package ru.intervi.nvomine.modules;

import ru.intervi.nvomine.Main;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Date;

import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import ru.intervi.littleconfig.utils.Utils;
import ru.intervi.littleconfig.FileStringList;

/**
 * создание точек с командами
 */
public class Points {
	public Points(Main m) {
		main = m;
		PATH = Utils.getFolderPath(this.getClass()) + File.separator + "NVomine" + File.separator + "points.txt";
		load();
	}
	
	private Main main;
	private ArrayList<UUID> list = new ArrayList<UUID>(); //список наблюдателей
	private ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>(); //список точек
	private ConcurrentHashMap<UUID, Date> delay = new ConcurrentHashMap<UUID, Date>(); //задержка для игроков
	private final String PATH; //файл с точками
	
	/**
	 * загрузить данные из файла
	 */
	public void load() {
		FileStringList fsl = new FileStringList();
		if (!(new File(PATH).isFile())) return;
		try {
			fsl.read(PATH);
		} catch(IOException e) {e.printStackTrace();}
		map.clear();
		for (String str : fsl.list) {
			String tloc = str.substring(str.indexOf('{'), (str.lastIndexOf('}')+1));
			String com = str.substring((str.lastIndexOf('}')+1));
			map.put(tloc, com);
		}
	}
	
	private void save() { //сохранить данные в файл
		if (map.isEmpty()) {
			new File(PATH).delete();
			return;
		}
		FileStringList fsl = new FileStringList();
		for (Entry<String, String> entry : map.entrySet()) {
			/*
			 * 0 - world
			 * 1 - x
			 * 2 - y
			 * 3 - z
			 * {world|x|y|x}command
			 */
			fsl.list.add((entry.getKey() + entry.getValue()));
		}
		try {
			fsl.write(PATH);
		} catch(IOException e) {e.printStackTrace();}
	}
	
	public void clear() {
		list.clear();
		map.clear();
		delay.clear();
	}
	
	/**
	 * добавить точку
	 * @param loc локация точки
	 * @param cmd команда
	 * @return true если точка добавлена; false если точка уже есть
	 */
	public boolean add(Location loc, String cmd) {
		if (map.containsKey(main.getLoc(loc))) return false;
		cmd = cmd.replaceAll("%world%", loc.getWorld().getName());
		cmd = cmd.replaceAll("%x%", String.valueOf(loc.getBlockX()));
		cmd = cmd.replaceAll("%y%", String.valueOf(loc.getBlockY()));
		cmd = cmd.replaceAll("%z%", String.valueOf(loc.getBlockZ()));
		map.put(main.getLoc(loc), cmd);
		save();
		return true;
	}
	
	/**
	 * удалить точку
	 * @param loc локация
	 * @return true, если удалось; false если точка не найдена
	 */
	public boolean remove(Location loc) {
		String tloc = main.getLoc(loc);
		if (map.containsKey(tloc)) {
			map.remove(tloc);
			save();
			return true;
		} else return false;
	}
	
	/**
	 * добавить игрока в режим наблюдения
	 * @param uuid UUID игрока
	 */
	public void addSpec(UUID uuid) {
		if (!list.contains(uuid)) list.add(uuid);
	}
	
	/**
	 * убрать игрока из режима наблюдения
	 * @param uuid UUID игрока
	 */
	public void remSpec(UUID uuid) {
		list.remove(uuid);
	}
	
	private void move(Player player, Location loc) { //обработка перемещений и выполнение команд от консоли
		if (!player.hasPermission("vom.points")) return;
		String tloc = main.getLoc(loc);
		if (!map.containsKey(tloc)) return;
		UUID uuid = player.getUniqueId();
		if (!list.contains(uuid) && delay.containsKey(uuid)) { //проверка на задержку
			Date d = delay.get(uuid);
			if ((System.currentTimeMillis() / 1000) - (d.getTime() / 1000) <= main.conf.delay) {
				delay.replace(uuid, new Date());
				return;
			}
		}
		String com = map.get(tloc).replaceAll("%name%", player.getName());
		if (list.contains(uuid)) player.sendMessage(main.mess.point.replaceAll("%com%", com));
		else {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), com);
			if (delay.containsKey(uuid)) delay.replace(uuid, new Date()); //обновление задержки
			else delay.put(uuid, new Date());
		}
	}
	
	public void onMove(PlayerMoveEvent event) { //обработка движений
		if (!main.conf.points) return;
		Location from = event.getFrom().getBlock().getLocation(),
				to = event.getTo().getBlock().getLocation();
		if (from.distance(to) >= 1) move(event.getPlayer(), to);
	}
	
	public void onQuit(PlayerQuitEvent event) { //очистка списка наблюдателей
		if (!main.conf.points) return;
		UUID uuid = event.getPlayer().getUniqueId();
		remSpec(uuid);
		delay.remove(uuid);
	}
	
	public void onTeleport(PlayerTeleportEvent event) { //обработка телепортаций
		if (!main.conf.points) return;
		move(event.getPlayer(), event.getTo().getBlock().getLocation());
	}
}
