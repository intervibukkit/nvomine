package ru.intervi.nvomine;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.World;
import org.bukkit.Location;
import org.bukkit.command.ConsoleCommandSender;

import ru.intervi.nvomine.modules.DayNight;

public class Main extends JavaPlugin implements Listener {
	public Config conf = new Config();
	public Mess mess = new Mess();
	private Events ev = new Events(this);
	private DayNight dn = new DayNight(this);
	
	public void info(String str) { //отправить сообщение в консоль
		getLogger().info(str);
	}
	
	@Override
	public void onEnable() { //активация плагина
		getServer().getPluginManager().registerEvents(ev, this);
		if (conf.daynight) dn.startTimer();
	}
	
	@Override
	public void onDisable() { //деактивация плагина
		ev.p.clear();
		ev.bh.clear();
		dn.stopTimer();
	}
	
	/**
	 * получить локацию в виде сжатой строки
	 * @param loc локация
	 * @return {world|x|y|x}
	 */
	public String getLoc(Location loc) {
		return '{' + loc.getWorld().getName() + '|' + String.valueOf(loc.getBlockX()) + '|' +
				String.valueOf(loc.getBlockY()) + '|' + String.valueOf(loc.getBlockZ()) + '}';
	}
	
	//обработка команд
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args == null || args.length == 0) {
			sender.sendMessage(mess.help);
			return true;
		}
		switch(args[0].toLowerCase()) {
		case "reload": //перезагрузка конфигов
			if (!sender.hasPermission("vom.admin") && !(sender instanceof ConsoleCommandSender)) {
				sender.sendMessage(mess.noperm);
				return true;
			}
			conf.load();
			mess.load();
			ev.p.load();
			ev.bh.load();
			dn.stopTimer();
			dn.startTimer();
			if (!conf.points) ev.p.clear();
			sender.sendMessage(mess.reload);
			break;
		case "points": //точки
			if (!sender.hasPermission("vom.points.set") && !sender.hasPermission("vom.admin")) {
				sender.sendMessage(mess.noperm);
				return true;
			}
			if (args.length < 2) {
				sender.sendMessage(mess.help);
				return true;
			}
			if (!conf.points) {
				sender.sendMessage(mess.modoff);
				return true;
			}
			if ((sender instanceof Player)) {
				Player player = (Player) sender;
				switch(args[1].toLowerCase()) {
				case "on": //включить режим наблюдения
					ev.p.addSpec(player.getUniqueId());
					sender.sendMessage(mess.seeon);
					break;
				case "off": //выключить режим наблюдения
					ev.p.remSpec(player.getUniqueId());
					sender.sendMessage(mess.seeoff);
					break;
				case "add": //добавить точку
					if (args.length < 3) {
						sender.sendMessage(mess.help);
						return true;
					}
					String com = args[2];
					for (int i = 3; i < args.length; i++) com += " " + args[i];
					if (ev.p.add(player.getLocation().getBlock().getLocation(), com)) sender.sendMessage(mess.padd);
					else sender.sendMessage(mess.pthere);
					break;
				case "remove": //удалить точку
					if (ev.p.remove(player.getLocation().getBlock().getLocation())) sender.sendMessage(mess.prem);
					else sender.sendMessage(mess.pnone);
					break;
				default:
					sender.sendMessage(mess.help);
				}
			} else sender.sendMessage(mess.ingame);
			break;
		case "boom": //создание взрыва
			if (!sender.hasPermission("vom.boom")) {
				sender.sendMessage(mess.noperm);
				return true;
			}
			if (args.length == 8) { //указание координат
				World world = Bukkit.getWorld(args[1]);
				if (world == null) {
					sender.sendMessage(mess.noworld);
					return true;
				}
				try {
					double x = Double.parseDouble(args[2]),
							y = Double.parseDouble(args[3]),
							z = Double.parseDouble(args[4]);
					float power = Float.parseFloat(args[5]);
					boolean fire = Boolean.parseBoolean(args[6]),
							blocks = Boolean.parseBoolean(args[7]);
					if (world.createExplosion(x, y, z, power, fire, blocks)) sender.sendMessage(mess.expl);
					else sender.sendMessage(mess.noexpl);
				} catch(Exception e) {sender.sendMessage(mess.novar);}
			} else if (args.length == 5) { //указание ника
				Player player = Bukkit.getPlayer(args[1]);
				if (player == null) {
					sender.sendMessage(mess.novar);
					return true;
				}
				Location loc = player.getLocation();
				try {
					float power = Float.parseFloat(args[2]);
					boolean fire = Boolean.parseBoolean(args[3]),
							blocks = Boolean.parseBoolean(args[4]);
					if (loc.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), power, fire, blocks))
						sender.sendMessage(mess.pexpl.replaceAll("%name%", player.getName()));
					else sender.sendMessage(mess.noexpl);
				} catch(Exception e) {sender.sendMessage(mess.novar);}
			} else sender.sendMessage(mess.help);
			break;
		case "hadd": //добавть принимающую отзывы воронку
			if (!sender.hasPermission("vom.hopper") && !sender.hasPermission("vom.admin")) {
				sender.sendMessage(mess.noperm);
				return true;
			}
			if ((sender instanceof Player)) {
				Player player = (Player) sender;
				if (ev.bh.add(player.getLocation())) sender.sendMessage(mess.hadd);
				else sender.sendMessage(mess.nohopper);
			} else sender.sendMessage(mess.ingame);
			break;
		case "hrem": //удалить воронку
			if (!sender.hasPermission("vom.hopper") && !sender.hasPermission("vom.admin")) {
				sender.sendMessage(mess.noperm);
				return true;
			}
			if ((sender instanceof Player)) {
				Player player = (Player) sender;
				if (ev.bh.remove(player.getLocation())) sender.sendMessage(mess.hdel);
				else sender.sendMessage(mess.nohdel);
			} else sender.sendMessage(mess.ingame);
			break;
		default:
			sender.sendMessage(mess.help);
		}
		return true;
	}
}
