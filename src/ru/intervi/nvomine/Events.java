package ru.intervi.nvomine;

import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import ru.intervi.nvomine.modules.*;

/**
 * обработка ивентов
 */
public class Events implements Listener {
	Events(Main m) {
		main = m;
		p = new Points(main);
		bh = new BookHopper(main);
		vs = new VSit(main);
		w = new Worlds(main);
	}
	
	private Main main;
	
	protected Points p; //точки с командами
	protected BookHopper bh; //принимающие воронки
	private VSit vs; //сажание жителей в транспорт
	private Worlds w; //выполнение команд при переходе в мир
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onMove(PlayerMoveEvent event) {
		p.onMove(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onQuit(PlayerQuitEvent event) {
		p.onQuit(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onTeleport(PlayerTeleportEvent event) {
		p.onTeleport(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void invPickup(InventoryPickupItemEvent event) {
		bh.invPickup(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBreak(BlockBreakEvent event) {
		bh.onBreak(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInteractE(PlayerInteractEntityEvent event) {
		vs.onInteractE(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onChangeWorld(PlayerChangedWorldEvent event) {
		w.onChangeWorld(event);
	}
}
